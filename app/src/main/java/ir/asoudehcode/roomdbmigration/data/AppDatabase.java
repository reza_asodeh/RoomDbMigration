package ir.asoudehcode.roomdbmigration.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import ir.asoudehcode.roomdbmigration.data.entity.Job;
import ir.asoudehcode.roomdbmigration.data.entity.User;
import ir.asoudehcode.roomdbmigration.data.dao.JobDao;
import ir.asoudehcode.roomdbmigration.data.dao.UserDao;

@Database(entities = {User.class, Job.class}, version = 5)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract UserDao userDao();

    public abstract JobDao jobDao();

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "mDatabase")
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5)
                    .build();
        }
        return instance;
    }


    /**
     * add phone column to table_user
     */
    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // add new column to table
            database.execSQL("ALTER TABLE table_user ADD COLUMN phone TEXT DEFAULT '09123456789'");


        }
    };

    /**
     * add address column to table_user
     */
    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            database.execSQL("ALTER TABLE table_user ADD COLUMN address TEXT DEFAULT 'tehran'");
        }
    };

    /**
     * create table_job table in database
     */
    private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE table_job (id INTEGER PRIMARY KEY NOT NULL,title TEXT,description TEXT DEFAULT '')");
        }
    };

    /**
     * create new table
     * copy all data from old table to new table
     * drop old table
     * rename new table
     */
    private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_table_job (id INTEGER PRIMARY KEY NOT NULL,title TEXT,description TEXT DEFAULT '')");
            database.execSQL("INSERT INTO new_table_job (id, title, description) SELECT id, title, description FROM table_job");
            database.execSQL("DROP TABLE table_job");
            database.execSQL("ALTER TABLE new_table_job RENAME TO table_job");
        }
    };


}