package ir.asoudehcode.roomdbmigration.data.repository;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import java.util.List;
import ir.asoudehcode.roomdbmigration.data.entity.Job;
import ir.asoudehcode.roomdbmigration.data.AppDatabase;
import ir.asoudehcode.roomdbmigration.data.dao.JobDao;

public class JobRepository {

    private JobDao jobDao;

    public JobRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        jobDao = database.jobDao();
    }


    public LiveData<List<Job>> getAllJobs() {
        return jobDao.getAllJobs();
    }

    public void insert(Job job) {
        new InsertJobAsyncTask(jobDao).execute(job);
    }


    private static class InsertJobAsyncTask extends AsyncTask<Job, Void, Void> {
        private JobDao jobDao;

        private InsertJobAsyncTask(JobDao jobDao) {
            this.jobDao = jobDao;
        }

        @Override
        protected Void doInBackground(Job... jobs) {
            jobDao.insert(jobs[0]);
            return null;
        }
    }


}