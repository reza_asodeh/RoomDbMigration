package ir.asoudehcode.roomdbmigration.data.repository;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import java.util.List;
import ir.asoudehcode.roomdbmigration.data.entity.User;
import ir.asoudehcode.roomdbmigration.data.AppDatabase;
import ir.asoudehcode.roomdbmigration.data.dao.UserDao;

public class UserRepository {

    private UserDao UserDao;

    public UserRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        UserDao = database.userDao();
    }


    public LiveData<List<User>> getAllUsers() {
        return UserDao.getAllUsers();
    }

    public void insert(User User) {
        new InsertUserAsyncTask(UserDao).execute(User);
    }


    private static class InsertUserAsyncTask extends AsyncTask<User, Void, Void> {
        private UserDao UserDao;

        private InsertUserAsyncTask(UserDao UserDao) {
            this.UserDao = UserDao;
        }

        @Override
        protected Void doInBackground(User... Users) {
            UserDao.insert(Users[0]);
            return null;
        }
    }


}