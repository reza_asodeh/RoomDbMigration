package ir.asoudehcode.roomdbmigration.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import java.util.List;
import ir.asoudehcode.roomdbmigration.data.entity.Job;

@Dao
public interface JobDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Job job);

    @Query("SELECT * FROM table_job")
    LiveData<List<Job>> getAllJobs();


}