package ir.asoudehcode.roomdbmigration.ui.job;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import ir.asoudehcode.roomdbmigration.R;
import ir.asoudehcode.roomdbmigration.data.entity.Job;

public class JobFragment extends Fragment {

    private String TAG = "all_tag";
    private JobViewModel dashboardViewModel;
    private int listSize = 0;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel = new ViewModelProvider(this).get(JobViewModel.class);
        View root = inflater.inflate(R.layout.fragment_job, container, false);


        getAllJobs();

        Button button = root.findViewById(R.id.button);

        button.setOnClickListener(v -> {
            int id = listSize + 1;
            Job newJob = new Job(id, "job" + id, "description" + id);
            dashboardViewModel.insert(newJob);
        });

        return root;
    }

    private void getAllJobs() {
        dashboardViewModel.getAllJobs().observe(getViewLifecycleOwner(), jobs -> {
            listSize = jobs.size();
            Log.i(TAG, "list size: " + listSize);

            for (int i = 0; i < jobs.size(); i++) {
                Log.i(TAG, "id: " + jobs.get(i).getId() + " -- " + jobs.get(i).getTitle() + " -- " + jobs.get(i).getDescription());
            }

        });
    }


}