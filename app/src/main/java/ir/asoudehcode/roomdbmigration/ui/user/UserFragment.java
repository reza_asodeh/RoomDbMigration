package ir.asoudehcode.roomdbmigration.ui.user;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ir.asoudehcode.roomdbmigration.R;
import ir.asoudehcode.roomdbmigration.data.entity.User;

public class UserFragment extends Fragment {

    private String TAG = "all_tag";
    private UserViewModel homeViewModel;
    private int listSize = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        View root = inflater.inflate(R.layout.fragment_user, container, false);


        getAllUsers();

        Button button = root.findViewById(R.id.button);

        button.setOnClickListener(v -> {
            int id = listSize + 1;
             User newUser = new User(id, "name " + id, "family " + id, "09123456789" , "tehran_iran");
            homeViewModel.insert(newUser);
        });


        return root;

    }

    private void getAllUsers() {
        homeViewModel.getAllUsers().observe(getViewLifecycleOwner(), users -> {
            listSize = users.size();
            Log.i(TAG, "list size: " + listSize);

            for (int i = 0; i < users.size(); i++) {
                 Log.i(TAG, "id: " + users.get(i).getUid() + " -- " + users.get(i).getFirstName() + "  -- " + users.get(i).getLastName() + "  -- " + users.get(i).getPhone()+ "  -- " + users.get(i).getAddress());
            }

        });
    }


}