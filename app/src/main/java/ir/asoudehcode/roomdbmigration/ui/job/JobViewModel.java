package ir.asoudehcode.roomdbmigration.ui.job;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;
import ir.asoudehcode.roomdbmigration.data.entity.Job;
import ir.asoudehcode.roomdbmigration.data.repository.JobRepository;

public class JobViewModel extends AndroidViewModel {

    private JobRepository jobRepository;

    public JobViewModel(@NonNull Application application) {
        super(application);
        jobRepository = new JobRepository(application);
    }

    public LiveData<List<Job>> getAllJobs() {
        return jobRepository.getAllJobs();
    }

    public void insert(Job job) {
        jobRepository.insert(job);
    }

}