package ir.asoudehcode.roomdbmigration.ui.user;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;
import ir.asoudehcode.roomdbmigration.data.entity.User;
import ir.asoudehcode.roomdbmigration.data.repository.UserRepository;

public class UserViewModel extends AndroidViewModel {

    private UserRepository userRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
    }

    public LiveData<List<User>> getAllUsers() {
        return userRepository.getAllUsers();
    }

    public void insert(User user) {
        userRepository.insert(user);
    }

}